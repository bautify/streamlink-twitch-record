#!/bin/bash
while true; do
streamlink -o [Twitchuser]_$(date +"%Y%m%d_%H%M").ts twitch.tv/[Twitchuser] best --twitch-disable-ads --twitch-disable-hosting --twitch-disable-reruns
echo "Stream is offline. Wait 120 seconds..."
sleep 120
done